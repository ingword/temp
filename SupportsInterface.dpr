program SupportsInterface;
{$APPTYPE CONSOLE}

uses
  SysUtils;

type
  ISomeInterface = interface
    ['{77CAEA2C-4ADD-41F1-BDB5-0614C242911C}']
    procedure SomeMethod;
  end; //

  TA = class(TObject, ISomeInterface, IUnknown { IUnknown тут СПЕЦИАЛЬНО опущен } )
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    function QueryInterface(const anID: TGUID; out anObj): hResult; virtual; stdcall;
    procedure SomeMethod;
  end; // TA

  TB = class(TA)
    function QueryInterface(const anID: TGUID; out anObj): hResult; override;
  end; // TB

  TC = class(TInterfacedObject, ISomeInterface)
    procedure SomeMethod;
  end; // TC

function TA._AddRef: Integer;
begin
  Result := -1;
end;

function TA._Release: Integer;
begin
  Result := -1;
end;

function TA.QueryInterface(const anID: TGUID; out anObj): hResult;
begin
  if Self.GetInterface(anID, anObj) then
    Result := S_Ok
  else
    Result := E_NoInterface;
end;

procedure TA.SomeMethod;
begin
  Write('A');
end;

function TB.QueryInterface(const anID: TGUID; out anObj): hResult;
begin
  if IsEqualGUID(anID, ISomeInterface) then
  begin
    Result := S_Ok;
    ISomeInterface(anObj) := TC.Create;
  end // IsEqualGUID(anID, ISomeInterface)
  else
    Result := inherited QueryInterface(anID, anObj);
end;

procedure TC.SomeMethod;
begin
  Write('C');
end;

var
  l_A: ISomeInterface;
  l_B: ISomeInterface;
  A: TA;
  B: TB;
begin
  A := TA.Create;
  B := TB.Create;
  if not Supports(A, ISomeInterface, l_A) then
    Assert(false);
  l_A.SomeMethod; // - в консоли видим A
  if not Supports(B, ISomeInterface, l_B) then
    Assert(false);
  l_B.SomeMethod;
  // - в консоли видим A, а "хотелось бы" - C
  Readln;
end.
